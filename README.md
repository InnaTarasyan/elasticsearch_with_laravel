# elasticsearch_with_laravel

## <b>Prerequisits</b>
 <ul>
   <li>Ensure you have PHP version 7.1.* </li>
   <li>Make sure that you have already installed ElasticSearch.</li>
 </ul>
 
##  <b>Running the site</b><br/>
 After you have cloned or downloaded the project, navigate to the corresponding directory
  <ul>
     <li>
     Install all the dependencies as specified in the <i>composer.json</i> file: <br/>
     composer install <br/>
     <li>Copy the <i>.env.example</i> file to the <i>.env</i> file, and set the corresponding keys: <br/>
<li>Run the following: <br/> php artisan key:generate</li> 
    <li>Execute the <i>migrations</i> and run the <i>seeders</i> <br/> php artisan migrate
         <br/>composer dump-autoload
         <br/>php artisan db:seed
     </li>



